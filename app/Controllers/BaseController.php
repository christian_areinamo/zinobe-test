<?php

namespace App\Controllers;

use Jenssegers\Blade\Blade;

class BaseController
{
    protected $src;

    public function __construct()
    {
        $this->src =  $_SERVER['DOCUMENT_ROOT'].'/';
    }

    public function view()
    {
        //return new Blade($this->src.'resources/views', 'cache');

        return new Blade($this->src.'resources/views', 'cache');
    }

}