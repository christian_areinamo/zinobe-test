<?php

namespace App\Controllers;

use Zend\Diactoros\Response\HtmlResponse;
use Jenssegers\Blade\Blade;
use App\Models\User;

/**
 * Class HomeController
 * @package App\Controllers
 */
class HomeController extends BaseController
{ 
    public function index()
    {
        $users = User::get();
        return new HtmlResponse($this->view()->make('list', ['users' => $users])->render());
    }
}