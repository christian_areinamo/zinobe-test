<?php

namespace App\Controllers;

use Zend\Diactoros\Response\HtmlResponse;
use Jenssegers\Blade\Blade;
use GuzzleHttp\Client;
use App\Models\User;
use Zend\Diactoros\Response\RedirectResponse;

class UserController extends BaseController
{ 
    public function login()
    {
        return new HtmlResponse($this->view()->make('login')->render());
    }

    public function create()
    {
        return new HtmlResponse($this->view()->make('register')->render());
    }

    public function save($request){
        $data = $request->getParsedBody();
        $responseMessage = "";
        

		$client = new Client();
		$res = $client->request('GET', 'http://www.mocky.io/v2/5d9f39263000005d005246ae');

		$responseData = json_decode($res->getBody());

		if (isset($responseData->objects)) {

			$foundKey = array_search($data['email'], array_column($responseData->objects, 'email'));

			if (isset($foundKey)) {

				$userApi = $responseData->objects[$foundKey];
				try{
					$user = new User();
					$user->job_title = $userApi->job_title;
					$user->email = $userApi->email;
					$user->first_name = $userApi->first_name;
					$user->last_name = $userApi->last_name;
					$user->document = $userApi->document;
					$user->phone_number = $userApi->phone_number;
					$user->country = $userApi->country;
					$user->state = $userApi->state;
					$user->city = $userApi->city;
				    $user->password = password_hash($data['password'], PASSWORD_DEFAULT);

				    $user->save();

				    $responseMessage = "registred";

				} catch (\Exception $e) {
				    $responseMessage = $e->getMessage();
				}				
			}
		}

		return new RedirectResponse('/login');
    }

    public function auth($request){		
	    $data = $request->getParsedBody();
	    $responseMessage = "";
	    $usuario = User::where('email', $data['email'])->first();

	    if($usuario) {
	        if (\password_verify($data['password'], $usuario->password)) {
	            $_SESSION['userId'] = $usuario->id;
	            return new RedirectResponse('/');
	        } else {
	            $responseMessage = "Correo o contraseña incorrecta";
	        }
	    } else {
	        $responseMessage = "Correo o contraseña incorrecta";
	    }

		return new RedirectResponse('/');
	}

	public function logout() {
		unset($_SESSION['userId']);
		return new RedirectResponse('/');
	}

}