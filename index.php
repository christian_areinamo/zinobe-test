<?php

require 'vendor/autoload.php';
require 'config/database.php';

use Aura\Router\RouterContainer; 

# Inicializa la sesion
session_start();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);


$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();
$map->get('index', '/', [
    'controller' => 'App\Controllers\HomeController',
    'action' => 'index',
    'auth' => true
]);

$map->get('register', '/register', [
    'controller' => 'App\Controllers\UserController',
    'action' => 'create'
]);

$map->get('login', '/login', [
    'controller' => 'App\Controllers\UserController',
    'action' => 'login'
]);

$map->post('auth', '/user/auth', [
    'controller' => 'App\Controllers\UserController',
    'action' => 'auth'
]);

$map->post('save', '/user/save', [
    'controller' => 'App\Controllers\UserController',
    'action' => 'save'
]);

$map->get('logout', '/user/logout', [
    'controller' => 'App\Controllers\UserController',
    'action' => 'logout'
]);


$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

if(!$route) { echo "No found"; } 
else {
    $routeData = $route->handler;
    $controllerName = new $routeData['controller'];
    $actionName = $routeData['action'];
    $authRequired = $routeData['auth'] ?? false;
    
    $sessionUserId = $_SESSION['userId'] ?? null;
    if($authRequired && !isset($sessionUserId)){
        $controllerName = "App\Controllers\UserController";
        $actionName = "login";
    }

    $controller = new $controllerName;
    $response = $controller->$actionName($request);

    # Obtenemos los encabezados de nuestra respuesta
    foreach($response->getHeaders() as $name => $values){
        foreach($values as $value){
            header(sprintf('%s: %s', $name, $value), false);
        }
    }
    http_response_code($response->getStatusCode());
    echo $response->getBody();
}