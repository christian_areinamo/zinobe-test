@extends('layout-auth')

@section('content')
  <form class="form" action="/user/auth" method="POST">
    <h1 class="h3 mb-3 fw-normal">Login</h1>

    @if (isset($message))
    	{{ $message }}
	@endif

    <div class="form-floating">
      <input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
      <label for="floatingInput">Email address</label>
    </div>
    <div class="form-floating">
      <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Password</label>
    </div>

	<a href="/register">registro</a>
	
	<hr>
    
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
  </form>
@endsection