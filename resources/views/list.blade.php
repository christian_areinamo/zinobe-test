@extends('layout')


@section('titulo')
	Listado de usuarios
@endsection

@section('content')
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">First</th>
	      <th scope="col">Last</th>
	      <th scope="col">Handle</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach ($users as $user)
			<tr>
				<th scope="row">1</th>
				<td>{{ $user->first_name }}</td>
				<td>{{ $user->last_name }}</td>
				<td>{{ $user->email }}</td>
			</tr>
		@endforeach
	  </tbody>
	</table>
@endsection