@extends('layout-auth')

@section('content')
  <form action="/user/save" method="POST">
    <h1 class="h3 mb-3 fw-normal">Registro</h1>

    @if (isset($message))
    	{{ $message }}
	@endif


    <div class="form-floating">
      <input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
      <label for="floatingInput">Email address</label>
    </div>

	<div class="form-floating">
      <input type="password" name="password" class="form-control" id="floatingInput" placeholder="Password">
      <label for="floatingInput">Password</label>
    </div>

    <a href="/login">login</a>
	
	<hr>
    
    <button class="w-100 btn btn-lg btn-primary" type="submit">registar</button>
  </form>
@endsection
